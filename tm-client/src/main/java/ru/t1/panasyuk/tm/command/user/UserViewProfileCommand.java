package ru.t1.panasyuk.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.t1.panasyuk.tm.dto.request.user.UserViewProfileRequest;
import ru.t1.panasyuk.tm.dto.response.user.UserViewProfileResponse;
import ru.t1.panasyuk.tm.enumerated.Role;

public final class UserViewProfileCommand extends AbstractUserCommand {

    @NotNull
    private final String NAME = "user-view-profile";

    @NotNull
    private final String DESCRIPTION = "View profile of current user.";

    @Override
    public void execute() {
        @NotNull final UserViewProfileRequest request = new UserViewProfileRequest(getToken());
        @NotNull final UserViewProfileResponse response = getAuthEndpoint().viewUserProfile(request);
        System.out.println(response.getUser());
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}