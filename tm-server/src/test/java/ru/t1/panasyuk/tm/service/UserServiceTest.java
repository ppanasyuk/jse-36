package ru.t1.panasyuk.tm.service;

import io.qameta.allure.junit4.DisplayName;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.t1.panasyuk.tm.api.repository.IProjectRepository;
import ru.t1.panasyuk.tm.api.repository.ISessionRepository;
import ru.t1.panasyuk.tm.api.repository.ITaskRepository;
import ru.t1.panasyuk.tm.api.repository.IUserRepository;
import ru.t1.panasyuk.tm.api.service.*;
import ru.t1.panasyuk.tm.enumerated.Role;
import ru.t1.panasyuk.tm.exception.entity.EntityNotFoundException;
import ru.t1.panasyuk.tm.exception.entity.UserNotFoundException;
import ru.t1.panasyuk.tm.exception.field.*;
import ru.t1.panasyuk.tm.model.User;
import ru.t1.panasyuk.tm.repository.ProjectRepository;
import ru.t1.panasyuk.tm.repository.SessionRepository;
import ru.t1.panasyuk.tm.repository.TaskRepository;
import ru.t1.panasyuk.tm.repository.UserRepository;

import java.util.ArrayList;
import java.util.List;

@DisplayName("Тестирование сервиса UserService")
public class UserServiceTest {

    @NotNull
    private IUserService userService;

    @NotNull
    private IAuthService authService;

    @NotNull
    private List<User> userList;

    @Before
    public void initService() {
        @NotNull final IProjectRepository projectRepository = new ProjectRepository();
        @NotNull final IUserRepository userRepository = new UserRepository();
        @NotNull final ITaskRepository taskRepository = new TaskRepository();
        @NotNull final ISessionRepository sessionRepository = new SessionRepository();
        @NotNull final IPropertyService propertyService = new PropertyService();
        @NotNull final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);
        userService = new UserService(propertyService, userRepository, projectTaskService);
        @NotNull final ISessionService sessionService = new SessionService(sessionRepository);
        authService = new AuthService(propertyService, userService, sessionService);
        @NotNull final User test = userService.create("TEST", "TEST", "ppanasyuk@t1-consulting.ru");
        @NotNull final User admin = userService.create("ADMIN", "ADMIN", Role.ADMIN);
        admin.setEmail("ADMIN@ADMIN");
        @NotNull final User user = userService.create("USER", "USER", Role.ADMIN);
        user.setEmail("USER@USER");
        userList = new ArrayList<>();
        userList.addAll(userService.findAll());
    }

    @Test
    @DisplayName("Добавление пользователя")
    public void AddTest() {
        int expectedNumberOfEntries = userService.getSize() + 1;
        @NotNull final User user = new User();
        user.setLogin("TEST_LOGIN");
        user.setFirstName("TEST_FIRSTNAME");
        user.setLastName("TEST_LASTNAME");
        user.setEmail("TEST@TEST");
        user.setRole(Role.USUAL);
        userService.add(user);
        Assert.assertEquals(expectedNumberOfEntries, userService.getSize());
    }

    @Test
    @DisplayName("Добавление списка пользователей")
    public void AddCollectionTest() {
        int expectedNumberOfEntries = userService.getSize() + 2;
        @NotNull final List<User> userList = new ArrayList<>();
        @NotNull final User firstUser = new User();
        firstUser.setLogin("TEST1_LOGIN");
        firstUser.setFirstName("TEST1_FIRSTNAME");
        firstUser.setLastName("TEST1_LASTNAME");
        firstUser.setEmail("TEST1@TEST");
        firstUser.setRole(Role.USUAL);
        userList.add(firstUser);
        @NotNull final User secondUser = new User();
        firstUser.setLogin("TEST2_LOGIN");
        firstUser.setFirstName("TEST2_FIRSTNAME");
        firstUser.setLastName("TEST2_LASTNAME");
        firstUser.setEmail("TEST2@TEST");
        firstUser.setRole(Role.USUAL);
        userList.add(secondUser);
        userService.add(userList);
        Assert.assertEquals(expectedNumberOfEntries, userService.getSize());
    }

    @Test
    @DisplayName("Создание пользователя")
    public void createTest() {
        @NotNull final String login = "LOGIN";
        @NotNull final String password = "PASS";
        @NotNull final User user = userService.create(login, password);
        @NotNull final String userId = user.getId();
        @Nullable final User newUser = userService.findOneById(userId);
        Assert.assertNotNull(newUser);
        Assert.assertEquals(login, newUser.getLogin());
    }

    @Test(expected = LoginEmptyException.class)
    @DisplayName("Создание пользователя с пустым логином")
    public void createLoginEmptyTestNegative() {
        userService.create("", "PASS");
    }

    @Test(expected = LoginExistsException.class)
    @DisplayName("Создание пользователя с существующим логином")
    public void createLoginExistsTestNegative() {
        userService.create("TEST", "PASS");
    }

    @Test(expected = PasswordEmptyException.class)
    @DisplayName("Создание пользователя с пустым паролем")
    public void createPasswordEmptyTestNegative() {
        userService.create("LOGIN", "");
    }

    @Test
    @DisplayName("Создание пользователя с Email")
    public void createWithEmailTest() {
        @NotNull final String login = "LOGIN";
        @NotNull final String password = "PASS";
        @NotNull final String email = "EMAIL";
        @NotNull final User user = userService.create(login, password, email);
        @NotNull final String userId = user.getId();
        @Nullable final User newUser = userService.findOneById(userId);
        Assert.assertNotNull(newUser);
        Assert.assertEquals(login, newUser.getLogin());
        Assert.assertEquals(email, newUser.getEmail());
    }

    @Test(expected = LoginEmptyException.class)
    @DisplayName("Создание пользователя с Email и пустым логином")
    public void createWithEmailLoginEmptyTestNegative() {
        userService.create("", "PASS", "EMAIL");
    }

    @Test(expected = LoginExistsException.class)
    @DisplayName("Создание пользователя с Email и существующим логином")
    public void createWithEmailLoginExistsTestNegative() {
        userService.create("TEST", "PASS", "EMAIL");
    }

    @Test(expected = PasswordEmptyException.class)
    @DisplayName("Создание пользователя с Email и пустым паролем")
    public void createWithEmailPasswordEmptyTestNegative() {
        userService.create("LOGIN", "", "EMAIL");
    }

    @Test(expected = EmailExistsException.class)
    @DisplayName("Создание пользователя с уже существующим Email")
    public void createWithEmailEmailExistsTestNegative() {
        userService.create("LOGIN", "PASS", "ppanasyuk@t1-consulting.ru");
    }

    @Test
    @DisplayName("Создание пользователя с ролью")
    public void createWithRole() {
        @NotNull final String login = "LOGIN";
        @NotNull final String password = "PASS";
        @NotNull final Role role = Role.USUAL;
        @NotNull final User user = userService.create(login, password, role);
        @NotNull final String userId = user.getId();
        @Nullable final User newUser = userService.findOneById(userId);
        Assert.assertNotNull(newUser);
        Assert.assertEquals(login, newUser.getLogin());
        Assert.assertEquals(role, newUser.getRole());
    }

    @Test(expected = LoginEmptyException.class)
    @DisplayName("Создание пользователя с ролью и пустым логином")
    public void createWithRoleLoginEmptyTestNegative() {
        userService.create("", "PASS", Role.USUAL);
    }

    @Test(expected = LoginExistsException.class)
    @DisplayName("Создание пользователя с ролью и существующим логином")
    public void createWithRoleLoginExistsTestNegative() {
        userService.create("TEST", "PASS", Role.USUAL);
    }

    @Test(expected = PasswordEmptyException.class)
    @DisplayName("Создание пользователя с ролью и пустым паролем")
    public void createWithRolePasswordEmptyTestNegative() {
        userService.create("LOGIN", "", Role.USUAL);
    }

    @Test
    @DisplayName("Удаление пользователей")
    public void clearTest() {
        int expectedNumberOfEntries = 0;
        Assert.assertTrue(userService.getSize() > 0);
        userService.clear();
        Assert.assertEquals(expectedNumberOfEntries, userService.getSize());
    }

    @Test
    @DisplayName("Установка списка пользователей")
    public void setTest() {
        int expectedNumberOfEntries = 2;
        @NotNull final List<User> userList = new ArrayList<>();
        @NotNull final User firstUser = new User();
        firstUser.setLogin("TEST1_LOGIN");
        firstUser.setFirstName("TEST1_FIRSTNAME");
        firstUser.setLastName("TEST1_LASTNAME");
        firstUser.setEmail("TEST1@TEST");
        firstUser.setRole(Role.USUAL);
        userList.add(firstUser);
        @NotNull final User secondUser = new User();
        firstUser.setLogin("TEST_LOGIN");
        firstUser.setFirstName("TEST_FIRSTNAME");
        firstUser.setLastName("TEST_LASTNAME");
        firstUser.setEmail("TEST@TEST");
        firstUser.setRole(Role.USUAL);
        userList.add(secondUser);
        userService.set(userList);
        Assert.assertEquals(expectedNumberOfEntries, userService.getSize());
    }

    @Test
    @DisplayName("Проверка существования пользователя по Id")
    public void existByIdTrueTest() {
        for (@NotNull final User user : userList) {
            final boolean isExist = userService.existsById(user.getId());
            Assert.assertTrue(isExist);
        }
    }

    @Test
    @DisplayName("Проверка несуществования пользователя по Id")
    public void existByIdFalseTest() {
        final boolean isExist = userService.existsById("123321");
        Assert.assertFalse(isExist);
    }

    @Test
    @DisplayName("Поиск всех пользователей")
    public void findAllTest() {
        @NotNull final List<User> users = userService.findAll();
        Assert.assertEquals(userList, users);
    }

    @Test
    @DisplayName("Поиск пользователя по логину")
    public void findByLoginTest() {
        Assert.assertTrue(userList.size() > 0);
        for (@NotNull final User user : userList) {
            @Nullable final  User foundUser = userService.findByLogin(user.getLogin());
            Assert.assertNotNull(foundUser);
        }
    }

    @Test(expected = LoginEmptyException.class)
    @DisplayName("Поиск пользователя по логину с пустым логином")
    public void findByLoginEmptyTestNegative() {
        @Nullable final User user = userService.findByLogin("");
    }

    @Test(expected = LoginEmptyException.class)
    @DisplayName("Поиск пользователя по логину с Null логином")
    public void findByLoginNullTestNegative() {
        @Nullable final User user = userService.findByLogin(null);
    }

    @Test
    @DisplayName("Поиск пользователя по Email")
    public void findByEmailTest() {
        Assert.assertTrue(userList.size() > 0);
        for (@NotNull final User user : userList) {
            @Nullable final  User foundUser = userService.findByEmail(user.getEmail());
            Assert.assertNotNull(foundUser);
        }
    }

    @Test
    @DisplayName("Поиск пользователя по Id")
    public void findOneByIdTest() {
        @Nullable User foundUser;
        for (@NotNull final User user : userList) {
            foundUser = userService.findOneById(user.getId());
            Assert.assertNotNull(foundUser);
        }
    }

    @Test
    @DisplayName("Поиск пользователя по Id равному Null")
    public void findOneByIdNullTest() {
        @Nullable final User foundUser = userService.findOneById(null);
        Assert.assertNull(foundUser);
    }

    @Test
    @DisplayName("Поиск пользователя по пустому Id")
    public void findOneByIdEmptyTest() {
        @Nullable final User foundUser = userService.findOneById("");
        Assert.assertNull(foundUser);
    }

    @Test
    @DisplayName("Поиск пользователя по индексу")
    public void findOneByIndexTest() {
        for (int i = 0; i < userList.size(); i++) {
            @Nullable final User user = userService.findOneByIndex(i);
            Assert.assertNotNull(user);
        }
    }

    @Test(expected = IndexIncorrectException.class)
    @DisplayName("Поиск пользователя по индексу превышающему границы")
    public void findOneByIndexIndexIncorrectNegative() {
        int index = userService.getSize() + 1;
        @Nullable final User user = userService.findOneByIndex(index);
    }

    @Test(expected = IndexIncorrectException.class)
    @DisplayName("Поиск пользователя по индексу равному Null")
    public void findOneByIndexNullIndexIncorrectNegative() {
        @Nullable final User user = userService.findOneByIndex(null);
    }

    @Test(expected = IndexIncorrectException.class)
    @DisplayName("Поиск пользователя по отрицательному индексу")
    public void findOneByIndexMinusIndexIncorrectNegative() {
        @Nullable final User user = userService.findOneByIndex(-1);
    }

    @Test
    @DisplayName("Получение количества пользователей")
    public void getSizeTest() {
        int expectedSize = userList.size();
        int actualSize = userService.getSize();
        Assert.assertEquals(expectedSize, actualSize);
    }

    @Test
    @DisplayName("Проверка существования логина в системе")
    public void isLoginExistTrueTest() {
        boolean isExist = userService.isLoginExist("ADMIN");
        Assert.assertTrue(isExist);
    }

    @Test
    @DisplayName("Проверка несуществования логина в системе")
    public void isLoginExistFalseTest() {
        boolean isExist = userService.isLoginExist("ABYRVALG");
        Assert.assertFalse(isExist);
    }

    @Test
    @DisplayName("Проверка существования логина Null в системе")
    public void isLoginExistNullFalseTest() {
        boolean isExist = userService.isLoginExist(null);
        Assert.assertFalse(isExist);
    }

    @Test
    @DisplayName("Проверка существования пустого логина в системе")
    public void isLoginExistEmptyFalseTest() {
        boolean isExist = userService.isLoginExist("");
        Assert.assertFalse(isExist);
    }

    @Test
    @DisplayName("Проверка существования Email в системе")
    public void isEmailExistTrueTest() {
        boolean isExist = userService.isEmailExist("ppanasyuk@t1-consulting.ru");
        Assert.assertTrue(isExist);
    }

    @Test
    @DisplayName("Проверка несуществования Email в системе")
    public void isEmailExistFalseTest() {
        boolean isExist = userService.isEmailExist("QWERTY@QWERTY");
        Assert.assertFalse(isExist);
    }

    @Test
    @DisplayName("Проверка существования Email равного Null в системе")
    public void isEmailExistNullFalseTest() {
        boolean isExist = userService.isEmailExist(null);
        Assert.assertFalse(isExist);
    }

    @Test
    @DisplayName("Проверка существования пустого Email в системе")
    public void isEmailExistEmptyFalseTest() {
        boolean isExist = userService.isEmailExist("");
        Assert.assertFalse(isExist);
    }

    @Test
    @DisplayName("Заблокировать пользователя по логину")
    public void lockUserByLoginTest() {
        userService.lockUserByLogin("TEST");
        @Nullable final User user = userService.findByLogin("TEST");
        Assert.assertNotNull(user);
        Assert.assertEquals(true, user.getLocked());
    }

    @Test(expected = LoginEmptyException.class)
    @DisplayName("Заблокировать пользователя по логину равному Null")
    public void lockUserByLoginLoginNullTestNegative() {
        userService.lockUserByLogin(null);
    }

    @Test(expected = LoginEmptyException.class)
    @DisplayName("Заблокировать пользователя по пустому логину")
    public void lockUserByLoginLoginEmptyTestNegative() {
        userService.lockUserByLogin("");
    }

    @Test(expected = UserNotFoundException.class)
    @DisplayName("Заблокировать пользователя по несуществующему логину")
    public void lockUserByLoginUserNotFoundTestNegative() {
        userService.lockUserByLogin("123");
    }

    @Test
    @DisplayName("Удаление пользователя по логину")
    public void removeByLoginTest() {
        @Nullable final User user = userService.findByLogin("TEST");
        Assert.assertNotNull(user);
        @Nullable User deletedUser = userService.removeByLogin("TEST");
        Assert.assertNotNull(deletedUser);
        deletedUser = userService.findByLogin("TEST");
        Assert.assertNull(deletedUser);
    }

    @Test(expected = LoginEmptyException.class)
    @DisplayName("Удаление пользователя по логину равному Null")
    public void removeByLoginLoginNullTestNegative() {
        userService.removeByLogin(null);
    }

    @Test(expected = LoginEmptyException.class)
    @DisplayName("Удаление пользователя по пустому логину")
    public void removeByLoginLoginEmptyTestNegative() {
        userService.removeByLogin("");
    }

    @Test(expected = UserNotFoundException.class)
    @DisplayName("Удаление пользователя по логину несуществующего в системе")
    public void removeByLoginUserNotFoundTestNegative() {
        userService.removeByLogin("123");
    }

    @Test
    @DisplayName("Удаление пользователя по Email")
    public void removeByEmailTest() {
        @Nullable final User user = userService.findByEmail("ppanasyuk@t1-consulting.ru");
        Assert.assertNotNull(user);
        @Nullable User deletedUser = userService.removeByEmail("ppanasyuk@t1-consulting.ru");
        Assert.assertNotNull(deletedUser);
        deletedUser = userService.findByEmail("ppanasyuk@t1-consulting.ru");
        Assert.assertNull(deletedUser);
    }

    @Test(expected = EmailEmptyException.class)
    @DisplayName("Удаление пользователя по Email равному Null")
    public void removeByEmailEmailNullTestNegative() {
        userService.removeByEmail(null);
    }

    @Test(expected = EmailEmptyException.class)
    @DisplayName("Удаление пользователя по пустому Email")
    public void removeByEmailEmailEmptyTestNegative() {
        userService.removeByEmail("");
    }

    @Test(expected = UserNotFoundException.class)
    @DisplayName("Удаление пользователя по Email несуществующему в системе")
    public void removeByEmailUserNotFoundTestNegative() {
        userService.removeByEmail("123");
    }

    @Test
    @DisplayName("Установить пользователю пароль")
    public void setPasswordTest() {
        @Nullable final User user = userService.findByLogin("TEST");
        Assert.assertNotNull(user);
        @Nullable final User updatedUser = userService.setPassword(user.getId(), "NEW_PASS");
        Assert.assertNotNull(updatedUser);
        @Nullable final String token = authService.login("TEST", "NEW_PASS");
        Assert.assertNotNull(token);
    }

    @Test(expected = IdEmptyException.class)
    @DisplayName("Установить пользователю пароль по Null id")
    public void setPasswordIdNullTestNegative() {
        userService.setPassword(null, "NEW_PASS");
    }

    @Test(expected = IdEmptyException.class)
    @DisplayName("Установить пользователю пароль по пустому Id")
    public void setPasswordIdEmptyTestNegative() {
        userService.setPassword("", "NEW_PASS");
    }

    @Test(expected = PasswordEmptyException.class)
    @DisplayName("Установить пользователю Null пароль")
    public void setPasswordPasswordNullTestNegative() {
        userService.setPassword("ID", null);
    }

    @Test(expected = PasswordEmptyException.class)
    @DisplayName("Установить пользователю пустой пароль")
    public void setPasswordPasswordEmptyTestNegative() {
        userService.setPassword("ID", "");
    }

    @Test(expected = UserNotFoundException.class)
    @DisplayName("Установить несуществующему пользователю пароль")
    public void setPasswordUserNotFoundTestNegative() {
        userService.setPassword("USER123", "NEW_PASS");
    }

    @Test
    @DisplayName("Удалить пользователя")
    public void removeTest() {
        for (@NotNull final User user : userList) {
            @NotNull final String userId = user.getId();
            @Nullable final User deletedUser = userService.remove(user);
            Assert.assertNotNull(deletedUser);
            @Nullable final User deletedUserInRepository = userService.findOneById(userId);
            Assert.assertNull(deletedUserInRepository);
        }
    }

    @Test(expected = EntityNotFoundException.class)
    @DisplayName("Удалить несуществующего пользователя")
    public void removeEntityNotFoundTestNegative() {
        @NotNull final User user = new User();
        userService.remove(user);
    }

    @Test(expected = EntityNotFoundException.class)
    @DisplayName("Удалить Null пользователя")
    public void removeEntityNullNotFoundTestNegative() {
        @Nullable final User deletedUser = userService.remove(null);
    }

    @Test
    @DisplayName("Удалить пользователя по Id")
    public void removeByIdTest() {
        for (@NotNull final User user : userList) {
            @NotNull final String userId = user.getId();
            @Nullable final User deletedUser = userService.removeById(userId);
            Assert.assertNotNull(deletedUser);
            @Nullable final User deletedUserInRepository = userService.findOneById(userId);
            Assert.assertNull(deletedUserInRepository);
        }
    }

    @Test(expected = IdEmptyException.class)
    @DisplayName("Удалить пользователя по Id равному Null")
    public void removeByIdIdNullTestNegative() {
        userService.removeById(null);
    }

    @Test(expected = IdEmptyException.class)
    @DisplayName("Удалить пользователя по пустому Id")
    public void removeByIdIdEmptyTestNegative() {
        userService.removeById("");
    }

    @Test(expected = EntityNotFoundException.class)
    @DisplayName("Удалить несуществующего пользователя")
    public void removeByIdEntityNotFoundTestNegative() {
        userService.removeById("123321");
    }

    @Test
    @DisplayName("Удалить пользователя по индексу")
    public void removeByIndexTest() {
        int index = userList.size();
        while (index > 0) {
            @Nullable final User deletedUser = userService.removeByIndex(index - 1);
            Assert.assertNotNull(deletedUser);
            @NotNull final String userId = deletedUser.getId();
            @Nullable final User deletedUserInRepository = userService.findOneById(userId);
            Assert.assertNull(deletedUserInRepository);
            index--;
        }
    }

    @Test(expected = IndexIncorrectException.class)
    @DisplayName("Удалить пользователя по индексу превышающему количество пользователей")
    public void removeByIndexIndexIncorrectTestNegative() {
        int index = userList.size() + 1;
        userService.removeByIndex(index);
    }

    @Test(expected = IndexIncorrectException.class)
    @DisplayName("Удалить пользователя по Null индексу")
    public void removeByIndexNullIndexIncorrectTestNegative() {
        userService.removeByIndex(null);
    }

    @Test(expected = IndexIncorrectException.class)
    @DisplayName("Удалить пользователя по отрицательному индексу")
    public void removeByIndexMinusIndexIncorrectTestNegative() {
        userService.removeByIndex(-1);
    }

    @Test
    @DisplayName("Разблокировать пользователя по логину")
    public void unlockUserByLoginTest() {
        userService.lockUserByLogin("TEST");
        @Nullable final User lockedUser = userService.findByLogin("TEST");
        Assert.assertNotNull(lockedUser);
        Assert.assertEquals(true, lockedUser.getLocked());
        userService.unlockUserByLogin("TEST");
        @Nullable final User unlockedUser = userService.findByLogin("TEST");
        Assert.assertNotNull(unlockedUser);
        Assert.assertEquals(false, unlockedUser.getLocked());
    }

    @Test(expected = LoginEmptyException.class)
    @DisplayName("Разблокировать пользователя по Null логину")
    public void unlockUserByLoginLoginNullTestNegative() {
        userService.unlockUserByLogin(null);
    }

    @Test(expected = LoginEmptyException.class)
    @DisplayName("Разблокировать пользователя по пустому логину")
    public void unlockUserByLoginLoginEmptyTestNegative() {
        userService.unlockUserByLogin("");
    }

    @Test(expected = UserNotFoundException.class)
    @DisplayName("Разблокировать несуществующего пользователя по логину")
    public void unlockUserByLoginUserNotFoundTestNegative() {
        userService.unlockUserByLogin("123");
    }

    @Test
    @DisplayName("Обновление пользователя")
    public void updateUser() {
        @NotNull final String firstName = "NEW_FIRST_NAME";
        @NotNull final String lastName = "NEW_LAST_NAME";
        @NotNull final String middleName = "NEW_MIDDLE_NAME";
        @Nullable final User user = userService.findByLogin("TEST");
        Assert.assertNotNull(user);
        userService.updateUser(user.getId(), firstName, lastName, middleName);
        @Nullable final User updatedUser = userService.findByLogin("TEST");
        Assert.assertNotNull(updatedUser);
        Assert.assertEquals(lastName, updatedUser.getLastName());
        Assert.assertEquals(firstName, updatedUser.getFirstName());
        Assert.assertEquals(middleName, updatedUser.getMiddleName());
    }

    @Test(expected = IdEmptyException.class)
    @DisplayName("Обновление пользователя по Null id")
    public void updateUserLoginNullTestNegative() {
        userService.updateUser(null, "FIRST_NAME", "LAST_NAME", "MIDDLE_NAME");
    }

    @Test(expected = IdEmptyException.class)
    @DisplayName("Обновление пользователя по пустому id")
    public void updateUserLoginEmptyTestNegative() {
        userService.updateUser("", "FIRST_NAME", "LAST_NAME", "MIDDLE_NAME");
    }

    @Test(expected = UserNotFoundException.class)
    @DisplayName("Обновление несуществующего пользователя")
    public void updateUserUserNotFoundTestNegative() {
        userService.updateUser("123321", "FIRST_NAME", "LAST_NAME", "MIDDLE_NAME");
    }

}