package ru.t1.panasyuk.tm.component;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.panasyuk.tm.api.endpoint.*;
import ru.t1.panasyuk.tm.api.repository.IProjectRepository;
import ru.t1.panasyuk.tm.api.repository.ISessionRepository;
import ru.t1.panasyuk.tm.api.repository.ITaskRepository;
import ru.t1.panasyuk.tm.api.repository.IUserRepository;
import ru.t1.panasyuk.tm.api.service.*;
import ru.t1.panasyuk.tm.endpoint.*;
import ru.t1.panasyuk.tm.enumerated.Role;
import ru.t1.panasyuk.tm.model.Project;
import ru.t1.panasyuk.tm.model.User;
import ru.t1.panasyuk.tm.repository.ProjectRepository;
import ru.t1.panasyuk.tm.repository.SessionRepository;
import ru.t1.panasyuk.tm.repository.TaskRepository;
import ru.t1.panasyuk.tm.repository.UserRepository;
import ru.t1.panasyuk.tm.service.*;
import ru.t1.panasyuk.tm.util.SystemUtil;

import javax.xml.ws.Endpoint;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

@NoArgsConstructor
public final class Bootstrap implements IServiceLocator {

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @Getter
    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @Getter
    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @Getter
    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    @Getter
    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @Getter
    @NotNull
    private final ILoggerService loggerService = new LoggerService(propertyService);

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @Getter
    @NotNull
    private final IUserService userService = new UserService(propertyService, userRepository, projectTaskService);

    @Getter
    @NotNull
    private final IDomainService domainService = new DomainService(this);

    @NotNull
    private final IAuthEndpoint authEndpoint = new AuthEndpoint(this);

    @NotNull
    private final ISystemEndpoint systemEndpoint = new SystemEndpoint(this);

    @NotNull
    private final IDomainEndpoint domainEndpoint = new DomainEndpoint(this);

    @NotNull
    private final IProjectEndpoint projectEndpoint = new ProjectEndpoint(this);

    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpoint(this);

    @NotNull
    private final IUserEndpoint userEndpoint = new UserEndpoint(this);

    @NotNull
    private final ISessionRepository sessionRepository = new SessionRepository();

    @NotNull
    private final ISessionService sessionService = new SessionService(sessionRepository);

    @Getter
    @NotNull
    private final IAuthService authService = new AuthService(propertyService, userService, sessionService);

    @NotNull
    private final Backup backup = new Backup(this);

    {
        registry(authEndpoint);
        registry(systemEndpoint);
        registry(projectEndpoint);
        registry(taskEndpoint);
        registry(domainEndpoint);
        registry(userEndpoint);
    }

    private void exit() {
        System.exit(0);
    }

    private void initBackup() {
        backup.start();
    }

    private void initDemoData() {
        @NotNull final User user1 = userService.create("PPANASYUK", "PPANASYUK", "ppanasyuk@t1-consulting.ru");
        @NotNull final User user2 = userService.create("SADMIN", "SADMIN", Role.ADMIN);

        @NotNull final Project project1 = projectService.create(user1.getId(), "Project 1", "Project for ppanasyuk");
        @NotNull final Project project2 = projectService.create(user2.getId(), "Project 2", "Project from sadmin");

        taskService.create(user1.getId(), "Task 1", "Task for project 1").setProjectId(project1.getId());
        taskService.create(user1.getId(), "Task 2", "Task for project 1").setProjectId(project1.getId());
        taskService.create(user2.getId(), "Task 3", "Task for project 2").setProjectId(project2.getId());
        taskService.create(user2.getId(), "Task 4", "Task for project 2").setProjectId(project2.getId());
        taskService.create(user2.getId(), "Task 5", "Test task");
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String fileName = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(fileName), pid.getBytes());
        @NotNull final File file = new File(fileName);
        file.deleteOnExit();
    }

    private void initLogger() {
        loggerService.info("*** WELCOME TO TASK MANAGER ***");
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutdown));
    }

    private void registry(@NotNull final Object endpoint) {
        @NotNull final String port = getPropertyService().getServerPort();
        @NotNull final String host = getPropertyService().getServerHost();
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @NotNull final String url = "http://" + host + ":" + port + "/" + name + "?wsdl";
        Endpoint.publish(url, endpoint);
        System.out.println(url);
    }

    public void start() {
        initPID();
        initDemoData();
        initLogger();
        initBackup();
    }

    private void prepareShutdown() {
        backup.stop();
        loggerService.info("*** TASK MANAGER IS SHUTTING DOWN ***");
    }

}