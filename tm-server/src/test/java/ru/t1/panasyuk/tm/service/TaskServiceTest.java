package ru.t1.panasyuk.tm.service;

import io.qameta.allure.junit4.DisplayName;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.t1.panasyuk.tm.api.repository.IProjectRepository;
import ru.t1.panasyuk.tm.api.repository.ITaskRepository;
import ru.t1.panasyuk.tm.api.repository.IUserRepository;
import ru.t1.panasyuk.tm.api.service.*;
import ru.t1.panasyuk.tm.enumerated.Role;
import ru.t1.panasyuk.tm.enumerated.Sort;
import ru.t1.panasyuk.tm.enumerated.Status;
import ru.t1.panasyuk.tm.exception.entity.EntityNotFoundException;
import ru.t1.panasyuk.tm.exception.entity.TaskNotFoundException;
import ru.t1.panasyuk.tm.exception.field.*;
import ru.t1.panasyuk.tm.model.Project;
import ru.t1.panasyuk.tm.model.Task;
import ru.t1.panasyuk.tm.model.User;
import ru.t1.panasyuk.tm.repository.ProjectRepository;
import ru.t1.panasyuk.tm.repository.TaskRepository;
import ru.t1.panasyuk.tm.repository.UserRepository;

import java.util.*;
import java.util.stream.Collectors;

@DisplayName("Тестирование сервиса TaskService")
public class TaskServiceTest {

    @NotNull
    private List<Task> taskList;

    @NotNull
    private IProjectService projectService;

    @NotNull
    private ITaskService taskService;

    @NotNull
    private User test;

    @NotNull
    private User admin;

    @Before
    public void initService() {
        @NotNull final IProjectRepository projectRepository = new ProjectRepository();
        @NotNull final IUserRepository userRepository = new UserRepository();
        @NotNull final ITaskRepository taskRepository = new TaskRepository();
        @NotNull final IPropertyService propertyService = new PropertyService();
        projectService = new ProjectService(projectRepository);
        taskService = new TaskService(taskRepository);
        @NotNull final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);
        @NotNull final IUserService userService = new UserService(propertyService, userRepository, projectTaskService);
        taskList = new ArrayList<>();
        test = userService.create("TEST", "TEST", "ppanasyuk@t1-consulting.ru");
        admin = userService.create("ADMIN", "ADMIN", Role.ADMIN);
        @NotNull final Project project1 = projectService.create(test.getId(), "Project 1", "Project for TEST");
        @NotNull final Project project2 = projectService.create(admin.getId(), "Project 2", "Project for ADMIN");
        @NotNull final Project project3 = projectService.create(test.getId(), "Project 3", "Project for TEST 2");
        @NotNull final Task task1 = taskService.create(test.getId(), "Task 1", "Task for project 1");
        task1.setProjectId(project1.getId());
        @NotNull final Task task2 = taskService.create(test.getId(), "Task 2", "Task for project 1");
        task2.setProjectId(project1.getId());
        @NotNull final Task task3 = taskService.create(admin.getId(), "Task 3", "Task for project 2");
        task3.setProjectId(project2.getId());
        @NotNull final Task task4 = taskService.create(admin.getId(), "Task 4", "Task for project 2");
        task4.setProjectId(project2.getId());
        @NotNull final Task task5 = taskService.create(admin.getId(), "Task 5", "Test task");
        taskList.add(task1);
        taskList.add(task2);
        taskList.add(task3);
        taskList.add(task4);
        taskList.add(task5);
    }

    @Test
    @DisplayName("Добавление задачи")
    public void AddTest() {
        int expectedNumberOfEntries = taskService.getSize() + 1;
        @NotNull final Task task = new Task();
        task.setUserId("45");
        task.setName("Test Add");
        task.setDescription("Test Add");
        taskService.add(task);
        Assert.assertEquals(expectedNumberOfEntries, taskService.getSize());
    }

    @Test
    @DisplayName("Добавление задачи с проверкой пользователя")
    public void AddForUserTest() {
        int expectedNumberOfEntries = taskService.getSize(test.getId()) + 1;
        @NotNull final Task task = new Task();
        task.setUserId(test.getId());
        task.setName("Test Add");
        task.setDescription("Test Add");
        taskService.add(test.getId(), task);
        Assert.assertEquals(expectedNumberOfEntries, taskService.getSize(test.getId()));
    }

    @Test
    @DisplayName("Добавление Null задачи с проверкой пользователя")
    public void AddNullForUserTest() {
        int expectedNumberOfEntries = taskService.getSize(test.getId());
        @Nullable final Task task = taskService.add(test.getId(), null);
        Assert.assertNull(task);
        Assert.assertEquals(expectedNumberOfEntries, taskService.getSize(test.getId()));
    }

    @Test
    @DisplayName("Добавление списка задач")
    public void AddCollectionTest() {
        int expectedNumberOfEntries = taskService.getSize() + 2;
        @NotNull final List<Task> taskList = new ArrayList<>();
        @NotNull final Task firstTask = new Task();
        firstTask.setUserId("45");
        firstTask.setName("Test Add 1");
        firstTask.setDescription("Test Add 2");
        taskList.add(firstTask);
        @NotNull final Task secondTask = new Task();
        secondTask.setUserId("45");
        secondTask.setName("Test Add 3");
        secondTask.setDescription("Test Add 4");
        taskList.add(secondTask);
        taskService.add(taskList);
        Assert.assertEquals(expectedNumberOfEntries, taskService.getSize());
    }

    @Test
    @DisplayName("Изменение статуса задачи по Id")
    public void changeTaskStatusByIdTest() {
        @NotNull final List<Task> tasks = taskService.findAll(test.getId());
        for (@NotNull final Task task : tasks) {
            @NotNull final String taskId = task.getId();
            @Nullable Task changedTask = taskService.changeTaskStatusById(test.getId(), taskId, Status.IN_PROGRESS);
            Assert.assertNotNull(changedTask);
            changedTask = taskService.findOneById(taskId);
            Assert.assertNotNull(changedTask);
            Assert.assertEquals(Status.IN_PROGRESS, changedTask.getStatus());
        }
    }

    @Test(expected = IdEmptyException.class)
    @DisplayName("Изменение статуса задачи по пустому Id")
    public void changeTaskStatusByIdTaskIdEmptyTestNegative() {
        @Nullable Task changedTask = taskService.changeTaskStatusById(test.getId(), "", Status.IN_PROGRESS);
    }

    @Test(expected = IdEmptyException.class)
    @DisplayName("Изменение статуса задачи по Null Id")
    public void changeTaskStatusByIdNullTaskIdEmptyTestNegative() {
        @Nullable Task changedTask = taskService.changeTaskStatusById(test.getId(), null, Status.IN_PROGRESS);
    }

    @Test(expected = StatusIncorrectException.class)
    @DisplayName("Изменение статуса задачи по Id на некорректный")
    public void changeTaskStatusByIdStatusIncorrectTestNegative() {
        @Nullable Task changedTask = taskService.changeTaskStatusById(test.getId(), "123", null);
    }

    @Test(expected = TaskNotFoundException.class)
    @DisplayName("Изменение статуса несуществующей")
    public void changeTaskStatusByIdTaskNotFoundTestNegative() {
        @Nullable Task changedTask = taskService.changeTaskStatusById(test.getId(), "123", Status.IN_PROGRESS);
    }

    @Test
    @DisplayName("Изменение статуса задачи по индексу")
    public void changeTaskStatusByIndexTest() {
        @NotNull final List<Task> tasks = taskService.findAll(test.getId());
        for (int i = 0; i < tasks.size(); i++) {
            @NotNull final Task task = tasks.get(i);
            @NotNull final String taskId = task.getId();
            @Nullable Task changedTask = taskService.changeTaskStatusByIndex(test.getId(), i, Status.IN_PROGRESS);
            Assert.assertNotNull(changedTask);
            changedTask = taskService.findOneById(taskId);
            Assert.assertNotNull(changedTask);
            Assert.assertEquals(Status.IN_PROGRESS, changedTask.getStatus());
        }
    }

    @Test(expected = IndexIncorrectException.class)
    @DisplayName("Изменение статуса задачи по Null индексу")
    public void changeTaskStatusByIndexIndexNullTestNegative() {
        @Nullable Task changedTask = taskService.changeTaskStatusByIndex(test.getId(), null, Status.IN_PROGRESS);
    }

    @Test(expected = IndexIncorrectException.class)
    @DisplayName("Изменение статуса задачи по отрицательному индексу")
    public void changeTaskStatusByIndexIndexMinusTestNegative() {
        @Nullable Task changedTask = taskService.changeTaskStatusByIndex(test.getId(), -1, Status.IN_PROGRESS);
    }

    @Test(expected = StatusIncorrectException.class)
    @DisplayName("Изменение статуса задачи по индексу на Null")
    public void changeTaskStatusByIIndexStatusIncorrectTestNegative() {
        @Nullable Task changedTask = taskService.changeTaskStatusByIndex(test.getId(), 0, null);
    }

    @Test(expected = IndexIncorrectException.class)
    @DisplayName("Изменение статуса задачи по индексу больше количества задач")
    public void changeTaskStatusByIndexIndexIncorrectTestNegative() {
        @Nullable Task changedTask = taskService.changeTaskStatusByIndex(test.getId(), 100, Status.IN_PROGRESS);
    }

    @Test
    @DisplayName("Удаление всех задач")
    public void clearTest() {
        int expectedNumberOfEntries = 0;
        Assert.assertTrue(taskService.getSize() > 0);
        taskService.clear();
        Assert.assertEquals(expectedNumberOfEntries, taskService.getSize());
    }

    @Test
    @DisplayName("Удаление всех задач пользователя")
    public void clearForUserTest() {
        int expectedNumberOfEntries = 0;
        Assert.assertTrue(taskService.getSize(test.getId()) > 0);
        taskService.clear(test.getId());
        Assert.assertEquals(expectedNumberOfEntries, taskService.getSize(test.getId()));
    }

    @Test
    @DisplayName("Создание задачи с именем и описанием")
    public void createTest() {
        int expectedNumberOfEntries = taskService.getSize(test.getId()) + 1;
        @NotNull final String name = "Task name";
        @NotNull final String description = "Task Description";
        @Nullable Task createdTask = taskService.create(test.getId(), name, description);
        @NotNull final String taskId = createdTask.getId();
        Assert.assertNotNull(createdTask);
        createdTask = taskService.findOneById(test.getId(), taskId);
        Assert.assertNotNull(createdTask);
        Assert.assertEquals(name, createdTask.getName());
        Assert.assertEquals(description, createdTask.getDescription());
        Assert.assertEquals(expectedNumberOfEntries, taskService.getSize(test.getId()));
    }

    @Test
    @DisplayName("Создание задачи с именем")
    public void createByNameTest() {
        int expectedNumberOfEntries = taskService.getSize(test.getId()) + 1;
        @NotNull final String name = "Task name";
        @Nullable Task createdTask = taskService.create(test.getId(), name);
        @NotNull final String taskId = createdTask.getId();
        Assert.assertNotNull(createdTask);
        createdTask = taskService.findOneById(test.getId(), taskId);
        Assert.assertNotNull(createdTask);
        Assert.assertEquals(name, createdTask.getName());
        Assert.assertEquals(expectedNumberOfEntries, taskService.getSize(test.getId()));
    }

    @Test(expected = NameEmptyException.class)
    @DisplayName("Создание задачи с пустым именем и описанием")
    public void createNameEmptyTestNegative() {
        taskService.create(test.getId(), "", "description");
    }

    @Test(expected = NameEmptyException.class)
    @DisplayName("Создание задачи с Null именем и описанием")
    public void createNullNameEmptyTestNegative() {
        taskService.create(test.getId(), null, "description");
    }

    @Test(expected = DescriptionEmptyException.class)
    @DisplayName("Создание задачи с пустым описанием")
    public void createDescriptionEmptyTestNegative() {
        taskService.create(test.getId(), "name", "");
    }

    @Test(expected = DescriptionEmptyException.class)
    @DisplayName("Создание задачи с Null описанием")
    public void createNullDescriptionEmptyTestNegative() {
        taskService.create(test.getId(), "name", null);
    }

    @Test(expected = NameEmptyException.class)
    @DisplayName("Создание задачи с пустым именем")
    public void createByNameNameEmptyTestNegative() {
        taskService.create(test.getId(), "");
    }

    @Test(expected = NameEmptyException.class)
    @DisplayName("Создание задачи с Null именем")
    public void createByNameNullNameEmptyTestNegative() {
        taskService.create(test.getId(), null);
    }

    @Test
    @DisplayName("Установить список задач")
    public void setTest() {
        int expectedNumberOfEntries = 2;
        @NotNull final List<Task> taskList = new ArrayList<>();
        @NotNull final Task firstTask = new Task();
        firstTask.setUserId("45");
        firstTask.setName("Test Add 1");
        firstTask.setDescription("Test Add 2");
        taskList.add(firstTask);
        @NotNull final Task secondTask = new Task();
        secondTask.setUserId("45");
        secondTask.setName("Test Add 3");
        secondTask.setDescription("Test Add 4");
        taskList.add(secondTask);
        taskService.set(taskList);
        Assert.assertEquals(expectedNumberOfEntries, taskService.getSize());
    }

    @Test
    @DisplayName("Проверка существования задачи по Id")
    public void existByIdTrueTest() {
        for (@NotNull final Task task : taskList) {
            final boolean isExist = taskService.existsById(task.getId());
            Assert.assertTrue(isExist);
        }
    }

    @Test
    @DisplayName("Проверка несуществования задачи по Id")
    public void existByIdFalseTest() {
        final boolean isExist = taskService.existsById("123321");
        Assert.assertFalse(isExist);
    }

    @Test
    @DisplayName("Проверка существования задачи по Id для пользователя")
    public void existByIdTrueForUserTest() {
        @NotNull final List<Task> tasksForTestUser = taskList
                .stream()
                .filter(m -> test.getId().equals(m.getUserId()))
                .collect(Collectors.toList());
        for (@NotNull final Task task : tasksForTestUser) {
            final boolean isExist = taskService.existsById(test.getId(), task.getId());
            Assert.assertTrue(isExist);
        }
    }

    @Test
    @DisplayName("Проверка несуществования задачи по Id для пользователя")
    public void existByIdFalseUserTest() {
        final boolean isExist = taskService.existsById("45", "123321");
        Assert.assertFalse(isExist);
    }

    @Test
    @DisplayName("Найти все задачи по Id проекта")
    public void findAllByProjectIdTest() {
        @Nullable final Project project = projectService.findOneByIndex(test.getId(), 0);
        Assert.assertNotNull(project);
        @NotNull final String projectId = project.getId();
        @NotNull final List<Task> tasksToFind = taskList
                .stream()
                .filter(m -> test.getId().equals(m.getUserId()))
                .filter(m -> projectId.equals(m.getProjectId()))
                .collect(Collectors.toList());
        @NotNull final List<Task> tasksByProjectId = taskService.findAllByProjectId(test.getId(), projectId);
        Assert.assertEquals(tasksToFind, tasksByProjectId);
    }

    @Test
    @DisplayName("Найти все задачи по пустому Id проекта")
    public void findAllByProjectIdEmptyTest() {
        @NotNull final List<Task> emptyList = Collections.emptyList();
        @NotNull final List<Task> tasksByProjectId = taskService.findAllByProjectId(test.getId(), "");
        Assert.assertEquals(emptyList, tasksByProjectId);
    }

    @Test
    @DisplayName("Найти все задачи по Null Id проекта")
    public void findAllByProjectIdNullTest() {
        @NotNull final List<Task> emptyList = Collections.emptyList();
        @NotNull final List<Task> tasksByProjectId = taskService.findAllByProjectId(test.getId(), null);
        Assert.assertEquals(emptyList, tasksByProjectId);
    }

    @Test
    @DisplayName("Найти все задачи")
    public void findAllTest() {
        @NotNull final List<Task> tasks = taskService.findAll();
        Assert.assertEquals(taskList, tasks);
    }

    @Test
    @DisplayName("Найти все задачи для пользователя")
    public void findAllForUserTest() {
        @NotNull final List<Task> tasksForTestUser = taskList
                .stream()
                .filter(m -> test.getId().equals(m.getUserId()))
                .collect(Collectors.toList());
        @NotNull final List<Task> tasks = taskService.findAll(test.getId());
        Assert.assertEquals(tasksForTestUser, tasks);
    }

    @Test
    @DisplayName("Найти все задачи с компаратором")
    public void findAllWithComparatorTest() {
        @Nullable Comparator<Task> comparator = Sort.BY_NAME.getComparator();
        @NotNull List<Task> tasks = taskService.findAll(comparator);
        Assert.assertEquals(taskList, tasks);
        comparator = Sort.BY_CREATED.getComparator();
        tasks = taskService.findAll(comparator);
        Assert.assertEquals(taskList, tasks);
        comparator = Sort.BY_STATUS.getComparator();
        tasks = taskService.findAll(comparator);
        Assert.assertEquals(taskList, tasks);
        comparator = null;
        tasks = taskService.findAll(comparator);
        Assert.assertEquals(taskList, tasks);
    }

    @Test
    @DisplayName("Найти все задачи с компаратором для пользователей")
    public void findAllWithComparatorForUserTest() {
        @NotNull final List<Task> tasksForTestUser = taskList
                .stream()
                .filter(m -> test.getId().equals(m.getUserId()))
                .collect(Collectors.toList());
        @Nullable Comparator<Task> comparator = Sort.BY_NAME.getComparator();
        @NotNull List<Task> tasks = taskService.findAll(test.getId(), comparator);
        Assert.assertEquals(tasksForTestUser, tasks);
        comparator = Sort.BY_CREATED.getComparator();
        tasks = taskService.findAll(test.getId(), comparator);
        Assert.assertEquals(tasksForTestUser, tasks);
        comparator = Sort.BY_STATUS.getComparator();
        tasks = taskService.findAll(test.getId(), comparator);
        Assert.assertEquals(tasksForTestUser, tasks);
    }

    @Test
    @DisplayName("Найти все задачи с сортировкой")
    public void findAllWithSortTest() {
        @NotNull List<Task> tasks = taskService.findAll(Sort.BY_NAME);
        Assert.assertEquals(taskList, tasks);
        tasks = taskService.findAll(Sort.BY_CREATED);
        Assert.assertEquals(taskList, tasks);
        tasks = taskService.findAll(Sort.BY_STATUS);
        Assert.assertEquals(taskList, tasks);
        @Nullable final Sort sort = null;
        tasks = taskService.findAll(sort);
        Assert.assertEquals(taskList, tasks);
    }

    @Test
    @DisplayName("Найти все задачи с сортировкой для пользователей")
    public void findAllWithSortForUserTest() {
        @NotNull final List<Task> tasksForTestUser = taskList
                .stream()
                .filter(m -> test.getId().equals(m.getUserId()))
                .collect(Collectors.toList());
        @NotNull List<Task> tasks = taskService.findAll(test.getId(), Sort.BY_NAME);
        Assert.assertEquals(tasksForTestUser, tasks);
        tasks = taskService.findAll(test.getId(), Sort.BY_CREATED);
        Assert.assertEquals(tasksForTestUser, tasks);
        tasks = taskService.findAll(test.getId(), Sort.BY_STATUS);
        Assert.assertEquals(tasksForTestUser, tasks);
        @Nullable final Sort sort = null;
        tasks = taskService.findAll(test.getId(), sort);
        Assert.assertEquals(tasksForTestUser, tasks);
    }

    @Test
    @DisplayName("Найти задачу по Id")
    public void findOneByIdTest() {
        @Nullable Task foundTask;
        for (@NotNull final Task task : taskList) {
            foundTask = taskService.findOneById(task.getId());
            Assert.assertNotNull(foundTask);
        }
    }

    @Test
    @DisplayName("Найти задачу по Id для пользователя")
    public void findOneByIdForUserTest() {
        @Nullable Task foundTask;
        @NotNull final List<Task> tasksForTestUser = taskList
                .stream()
                .filter(m -> test.getId().equals(m.getUserId()))
                .collect(Collectors.toList());
        for (@NotNull final Task task : tasksForTestUser) {
            foundTask = taskService.findOneById(test.getId(), task.getId());
            Assert.assertNotNull(foundTask);
        }
    }

    @Test
    @DisplayName("Найти задачу по Null Id")
    public void findOneByIdNullTest() {
        @Nullable final Task foundTask = taskService.findOneById(null);
        Assert.assertNull(foundTask);
    }

    @Test
    @DisplayName("Найти задачу по пустому Id")
    public void findOneByIdEmptyTest() {
        @Nullable final Task foundTask = taskService.findOneById("");
        Assert.assertNull(foundTask);
    }

    @Test
    @DisplayName("Найти задачу по Null Id для пользователя")
    public void findOneByIdNullForUserTest() {
        @Nullable final Task foundTask = taskService.findOneById(test.getId(), null);
        Assert.assertNull(foundTask);
    }

    @Test
    @DisplayName("Найти задачу по индексу")
    public void findOneByIndexTest() {
        for (int i = 0; i < taskList.size(); i++) {
            @Nullable final Task task = taskService.findOneByIndex(i);
            Assert.assertNotNull(task);
        }
    }

    @Test
    @DisplayName("Найти задачу по индексу для пользователя")
    public void findOneByIndexForUserTest() {
        @NotNull final List<Task> tasksForTestUser = taskList
                .stream()
                .filter(m -> test.getId().equals(m.getUserId()))
                .collect(Collectors.toList());
        for (int i = 0; i < tasksForTestUser.size(); i++) {
            @Nullable final Task task = taskService.findOneByIndex(test.getId(), i);
            Assert.assertNotNull(task);
        }
    }

    @Test(expected = IndexIncorrectException.class)
    @DisplayName("Найти задачу по индексу превышающему количество задач для пользователя")
    public void findOneByIndexForUserIndexIncorrectNegative() {
        int index = taskService.getSize(test.getId()) + 1;
        @Nullable final Task task = taskService.findOneByIndex(test.getId(), index);
    }

    @Test(expected = IndexIncorrectException.class)
    @DisplayName("Найти задачу по Null индексу для пользователя")
    public void findOneByIndexForUserNullIndexIncorrectNegative() {
        @Nullable final Task task = taskService.findOneByIndex(test.getId(), null);
    }

    @Test(expected = IndexIncorrectException.class)
    @DisplayName("Найти задачу по отрицательному индексу для пользователя")
    public void findOneByIndexForUserMinusIndexIncorrectNegative() {
        @Nullable final Task task = taskService.findOneByIndex(test.getId(), -1);
    }

    @Test(expected = IndexIncorrectException.class)
    @DisplayName("Найти задачу по индексу превышающему количество задач")
    public void findOneByIndexIndexIncorrectNegative() {
        int index = taskService.getSize() + 1;
        @Nullable final Task task = taskService.findOneByIndex(index);
    }

    @Test(expected = IndexIncorrectException.class)
    @DisplayName("Найти задачу по Null индексу")
    public void findOneByIndexNullIndexIncorrectNegative() {
        @Nullable final Task task = taskService.findOneByIndex(null);
    }

    @Test(expected = IndexIncorrectException.class)
    @DisplayName("Найти задачу по отрицательному индексу")
    public void findOneByIndexMinusIndexIncorrectNegative() {
        @Nullable final Task task = taskService.findOneByIndex(-1);
    }

    @Test
    @DisplayName("Получить количество задач")
    public void getSizeTest() {
        int expectedSize = taskList.size();
        int actualSize = taskService.getSize();
        Assert.assertEquals(expectedSize, actualSize);
    }

    @Test
    @DisplayName("Получить количество задач для пользователя")
    public void getSizeForUserTest() {
        int expectedSize = (int) taskList
                .stream()
                .filter(m -> test.getId().equals(m.getUserId()))
                .count();
        int actualSize = taskService.getSize(test.getId());
        Assert.assertEquals(expectedSize, actualSize);
    }

    @Test
    @DisplayName("Удалить задачу")
    public void removeTest() {
        for (@NotNull final Task task : taskList) {
            @NotNull final String taskId = task.getId();
            @Nullable final Task deletedTask = taskService.remove(task);
            Assert.assertNotNull(deletedTask);
            @Nullable final Task deletedTaskInRepository = taskService.findOneById(taskId);
            Assert.assertNull(deletedTaskInRepository);
        }
    }

    @Test(expected = EntityNotFoundException.class)
    @DisplayName("Удалить несуществующую задачу")
    public void removeEntityNotFoundTestNegative() {
        @NotNull final Task task = new Task();
        taskService.remove(task);
    }

    @Test(expected = EntityNotFoundException.class)
    @DisplayName("Удалить Null задачу")
    public void removeEntityNullNotFoundTestNegative() {
        @Nullable final Task deletedTask = taskService.remove(null);
    }

    @Test
    @DisplayName("Удалить все задачи методом removeAll")
    public void removeAllTest() {
        int expectedNumberOfEntries = 0;
        @NotNull final List<Task> tasksForTestUser = taskList
                .stream()
                .filter(m -> test.getId().equals(m.getUserId()))
                .collect(Collectors.toList());
        taskService.removeAll(tasksForTestUser);
        Assert.assertEquals(expectedNumberOfEntries, taskService.getSize(test.getId()));
    }

    @Test
    @DisplayName("Удалить задачу по Id")
    public void removeByIdTest() {
        for (@NotNull final Task task : taskList) {
            @NotNull final String taskId = task.getId();
            @Nullable final Task deletedTask = taskService.removeById(taskId);
            Assert.assertNotNull(deletedTask);
            @Nullable final Task deletedTaskInRepository = taskService.findOneById(taskId);
            Assert.assertNull(deletedTaskInRepository);
        }
    }

    @Test
    @DisplayName("Удалить задачу по Id для пользователя")
    public void removeByIdForUserTest() {
        @NotNull final List<Task> tasksForTestUser = taskList
                .stream()
                .filter(m -> test.getId().equals(m.getUserId()))
                .collect(Collectors.toList());
        for (@NotNull final Task task : tasksForTestUser) {
            @NotNull final String taskId = task.getId();
            @Nullable final Task deletedTask = taskService.removeById(test.getId(), taskId);
            Assert.assertNotNull(deletedTask);
            @Nullable final Task deletedTaskInRepository = taskService.findOneById(test.getId(), taskId);
            Assert.assertNull(deletedTaskInRepository);
        }
    }

    @Test(expected = IdEmptyException.class)
    @DisplayName("Удалить задачу по Null Id")
    public void removeByIdIdNullTestNegative() {
        taskService.removeById(null);
    }

    @Test(expected = IdEmptyException.class)
    @DisplayName("Удалить задачу по пустому Id")
    public void removeByIdIdEmptyTestNegative() {
        taskService.removeById("");
    }

    @Test(expected = EntityNotFoundException.class)
    @DisplayName("Удалить несуществующую")
    public void removeByIdEntityNotFoundTestNegative() {
        taskService.removeById("123321");
    }

    @Test(expected = IdEmptyException.class)
    @DisplayName("Удалить задачу по Null Id для пользователя")
    public void removeByIdForUserIdNullTestNegative() {
        taskService.removeById(test.getId(), null);
    }

    @Test(expected = IdEmptyException.class)
    @DisplayName("Удалить задачу по пустому Id для пользователя")
    public void removeByIdForUserIdEmptyTestNegative() {
        taskService.removeById(test.getId(), "");
    }

    @Test(expected = EntityNotFoundException.class)
    @DisplayName("Удалить несуществующую задачу по Id для пользователя")
    public void removeByIdForUserEntityNotFoundTestNegative() {
        taskService.removeById(test.getId(), "123321");
    }

    @Test
    @DisplayName("Удалить задачу по индексу")
    public void removeByIndexTest() {
        int index = taskList.size();
        while (index > 0) {
            @Nullable final Task deletedTask = taskService.removeByIndex(index - 1);
            Assert.assertNotNull(deletedTask);
            @NotNull final String taskId = deletedTask.getId();
            @Nullable final Task deletedTaskInRepository = taskService.findOneById(taskId);
            Assert.assertNull(deletedTaskInRepository);
            index--;
        }
    }

    @Test
    @DisplayName("Удалить задачу по индексу для пользователя")
    public void removeByIndexForUserTest() {
        int index = (int) taskList
                .stream()
                .filter(m -> test.getId().equals(m.getUserId()))
                .count();
        while (index > 0) {
            @Nullable final Task deletedTask = taskService.removeByIndex(test.getId(), index - 1);
            Assert.assertNotNull(deletedTask);
            @NotNull final String taskId = deletedTask.getId();
            @Nullable final Task deletedTaskInRepository = taskService.findOneById(test.getId(), taskId);
            Assert.assertNull(deletedTaskInRepository);
            index--;
        }
    }

    @Test(expected = IndexIncorrectException.class)
    @DisplayName("Удалить задачу по индексу больше количества задач")
    public void removeByIndexIndexIncorrectTestNegative() {
        int index = taskList.size() + 1;
        taskService.removeByIndex(index);
    }

    @Test(expected = IndexIncorrectException.class)
    @DisplayName("Удалить задачу по Null индексу")
    public void removeByIndexNullIndexIncorrectTestNegative() {
        taskService.removeByIndex(null);
    }

    @Test(expected = IndexIncorrectException.class)
    @DisplayName("Удалить задачу по отрицательному индексу ноля")
    public void removeByIndexMinusIndexIncorrectTestNegative() {
        taskService.removeByIndex(-1);
    }

    @Test(expected = IndexIncorrectException.class)
    @DisplayName("Удалить задачу по индексу больше количества задач для пользователя")
    public void removeByIndexForUserIndexIncorrectTestNegative() {
        int index = taskList.size() + 1;
        taskService.removeByIndex(test.getId(), index);
    }

    @Test(expected = IndexIncorrectException.class)
    @DisplayName("Удалить задачу по Null индексу для пользователя")
    public void removeByIndexNullForUserIndexIncorrectTestNegative() {
        taskService.removeByIndex(test.getId(), null);
    }

    @Test(expected = IndexIncorrectException.class)
    @DisplayName("Удалить задачу по отрицательному индексу для пользователя")
    public void removeByIndexMinusForUserIndexIncorrectTestNegative() {
        taskService.removeByIndex(test.getId(), -1);
    }

    @Test
    @DisplayName("Обновить задачу по Id")
    public void updateByIdTest() {
        @NotNull final List<Task> tasks = taskService.findAll(test.getId());
        @NotNull String name = "";
        @NotNull String description = "";
        int index = 0;
        for (@NotNull final Task task : tasks) {
            @NotNull final String taskId = task.getId();
            name = "name " + index;
            description = "description" + index;
            @Nullable Task updatedTask = taskService.updateById(test.getId(), taskId, name, description);
            Assert.assertNotNull(updatedTask);
            updatedTask = taskService.findOneById(taskId);
            Assert.assertNotNull(updatedTask);
            Assert.assertEquals(name, updatedTask.getName());
            Assert.assertEquals(description, updatedTask.getDescription());
            index++;
        }
    }

    @Test
    @DisplayName("Обновить задачу по индексу")
    public void updateByIndexTest() {
        @NotNull final List<Task> tasks = taskService.findAll(test.getId());
        @NotNull String name = "";
        @NotNull String description = "";
        int index = 0;
        for (@NotNull final Task task : tasks) {
            @NotNull final String taskId = task.getId();
            name = "name " + index;
            description = "description" + index;
            @Nullable Task updatedTask = taskService.updateByIndex(test.getId(), index, name, description);
            Assert.assertNotNull(updatedTask);
            updatedTask = taskService.findOneById(taskId);
            Assert.assertNotNull(updatedTask);
            Assert.assertEquals(name, updatedTask.getName());
            Assert.assertEquals(description, updatedTask.getDescription());
            index++;
        }
    }

    @Test(expected = IdEmptyException.class)
    @DisplayName("Обновить задачу по пустому Id")
    public void UpdateByIdIdEmptyTestNegative() {
        taskService.updateById(test.getId(), "", "name", "description");
    }

    @Test(expected = IdEmptyException.class)
    @DisplayName("Обновить задачу по Null Id")
    public void UpdateByIdNullIdEmptyTestNegative() {
        taskService.updateById(test.getId(), null, "name", "description");
    }

    @Test(expected = NameEmptyException.class)
    @DisplayName("Обновить задачу по Id и пустым именем")
    public void UpdateByIdNameEmptyTestNegative() {
        taskService.updateById(test.getId(), "id", "", "description");
    }

    @Test(expected = NameEmptyException.class)
    @DisplayName("Обновить задачу по Id и Null именем")
    public void UpdateByIdNullNameEmptyTestNegative() {
        taskService.updateById(test.getId(), "id", null, "description");
    }

    @Test(expected = TaskNotFoundException.class)
    @DisplayName("Обновить несуществующую задачу по Id")
    public void UpdateByIdTaskNotFoundTestNegative() {
        taskService.updateById(test.getId(), "123", "name", "description");
    }

    @Test(expected = IndexIncorrectException.class)
    @DisplayName("Обновить задачу по Null индексу")
    public void UpdateByIndexIndexNullTestNegative() {
        taskService.updateByIndex(test.getId(), null, "name", "description");
    }

    @Test(expected = IndexIncorrectException.class)
    @DisplayName("Обновить задачу по отрицательному индексу")
    public void UpdateByIndexMinusTestNegative() {
        taskService.updateByIndex(test.getId(), -1, "name", "description");
    }

    @Test(expected = IndexIncorrectException.class)
    @DisplayName("Обновить задачу по индексу больше количества задач")
    public void UpdateByIndexIndexIncorrectTestNegative() {
        taskService.updateByIndex(test.getId(), 100, "", "description");
    }

    @Test(expected = NameEmptyException.class)
    @DisplayName("Обновить задачу по индексу с Null именем")
    public void UpdateByIndexNullNameEmptyTestNegative() {
        taskService.updateByIndex(test.getId(), 0, null, "description");
    }

    @Test(expected = NameEmptyException.class)
    @DisplayName("Обновить задачу по индексу с пустым именем")
    public void UpdateByIndexNameEmptyTestNegative() {
        taskService.updateByIndex(test.getId(), 0, "", "description");
    }

}