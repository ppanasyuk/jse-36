package ru.t1.panasyuk.tm.repository;

import io.qameta.allure.junit4.DisplayName;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.t1.panasyuk.tm.api.repository.ISessionRepository;
import ru.t1.panasyuk.tm.model.Session;
import ru.t1.panasyuk.tm.util.SystemUtil;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@DisplayName("Тестирование репозитория сессий")
public class SessionRepositoryTest {

    private final int NUMBER_OF_ENTRIES = 10;

    @NotNull
    private final String USER_1_ID = SystemUtil.generateGuid();

    @NotNull
    private final String USER_2_ID = SystemUtil.generateGuid();

    @NotNull
    private List<Session> sessionList;

    @NotNull
    private ISessionRepository sessionRepository;

    @Before
    public void initRepository() {
        sessionList = new ArrayList<>();
        sessionRepository = new SessionRepository();
        for (int i = 0; i < NUMBER_OF_ENTRIES; i++) {
            @NotNull final Session session = new Session();
            if (i < 5) session.setUserId(USER_1_ID);
            else session.setUserId(USER_2_ID);
            sessionList.add(session);
            sessionRepository.add(session);
        }
    }

    @Test
    @DisplayName("Добавить сессию")
    public void addTest() {
        int expectedNumberOfEntries = sessionRepository.getSize() + 1;
        @NotNull final Session session = new Session();
        sessionRepository.add(USER_1_ID, session);
        Assert.assertEquals(expectedNumberOfEntries, sessionRepository.getSize());
        @Nullable final Session createdSession = sessionRepository.findOneByIndex(sessionRepository.getSize() - 1);
        Assert.assertNotNull(createdSession);
        Assert.assertEquals(USER_1_ID, createdSession.getUserId());
    }

    @Test
    @DisplayName("Добавить Null сессию")
    public void addNullTest() {
        @Nullable final Session createdSession = sessionRepository.add(USER_1_ID, null);
        Assert.assertNull(createdSession);
    }

    @Test
    @DisplayName("Добавить список сессий")
    public void addAllTest() {
        int expectedNumberOfEntries = sessionRepository.getSize() + 2;
        @NotNull final List<Session> sessions = new ArrayList<>();
        @NotNull final Session firstSession = new Session();
        sessions.add(firstSession);
        @NotNull final Session secondSession = new Session();
        sessions.add(secondSession);
        @NotNull final Collection<Session> addedSessions = sessionRepository.add(sessions);
        Assert.assertTrue(addedSessions.size() > 0);
        int actualNumberOfEntries = sessionRepository.getSize();
        Assert.assertEquals(expectedNumberOfEntries, actualNumberOfEntries);
    }

    @Test
    @DisplayName("Удалить все сессии")
    public void clearTest() {
        int expectedNumberOfEntries = 0;
        sessionRepository.clear();
        Assert.assertEquals(expectedNumberOfEntries, sessionRepository.getSize());
    }

    @Test
    @DisplayName("Удалить все сессии для пользователя")
    public void clearForUserTest() {
        int expectedNumberOfEntries = 0;
        sessionRepository.clear(USER_1_ID);
        Assert.assertEquals(expectedNumberOfEntries, sessionRepository.getSize(USER_1_ID));
    }

    @Test
    @DisplayName("Проверка существования сессии по Id")
    public void existByIdTrueTest() {
        boolean expectedResult = true;
        @Nullable final Session session = sessionRepository.findOneByIndex(0);
        Assert.assertNotNull(session);
        @Nullable final String sessionId = session.getId();
        Assert.assertEquals(expectedResult, sessionRepository.existsById(sessionId));
    }

    @Test
    @DisplayName("Проверка несуществования сессии по Id")
    public void existByIdFalseTest() {
        boolean expectedResult = false;
        Assert.assertEquals(expectedResult, sessionRepository.existsById("qwerty"));
    }

    @Test
    @DisplayName("Проверка существования сессии по Id для пользователя")
    public void existByIdForUserTrueTest() {
        boolean expectedResult = true;
        @Nullable final Session session = sessionRepository.findOneByIndex(USER_1_ID, 0);
        Assert.assertNotNull(session);
        @Nullable final String sessionId = session.getId();
        Assert.assertEquals(expectedResult, sessionRepository.existsById(USER_1_ID, sessionId));
    }

    @Test
    @DisplayName("Проверка несуществования сессии по Id для пользователя")
    public void existByIdForUserFalseTest() {
        boolean expectedResult = false;
        Assert.assertEquals(expectedResult, sessionRepository.existsById(USER_1_ID, "qwerty"));
    }

    @Test
    @DisplayName("Поиск всех сессий")
    public void findAllTest() {
        @NotNull final List<Session> sessions = sessionRepository.findAll();
        Assert.assertEquals(sessionList, sessions);
    }

    @Test
    @DisplayName("Поиск всех сессий для пользователя")
    public void findAllForUserTest() {
        @NotNull List<Session> sessionListForUser = sessionList
                .stream()
                .filter(m -> USER_1_ID.equals(m.getUserId()))
                .collect(Collectors.toList());
        @NotNull final List<Session> sessions = sessionRepository.findAll(USER_1_ID);
        Assert.assertEquals(sessionListForUser, sessions);
    }

    @Test
    @DisplayName("Поиск сессии по Id")
    public void findOneByIdTest() {
        @Nullable Session session;
        for (int i = 0; i < sessionList.size(); i++) {
            session = sessionRepository.findOneByIndex(i);
            Assert.assertNotNull(session);
            @NotNull final String sessionId = session.getId();
            @Nullable final Session foundSession = sessionRepository.findOneById(sessionId);
            Assert.assertNotNull(foundSession);
        }
    }

    @Test
    @DisplayName("Поиск сессии по Null Id")
    public void findOneByIdNullTest() {
        @Nullable final Session foundSession = sessionRepository.findOneById("qwerty");
        Assert.assertNull(foundSession);
        @Nullable final Session foundSessionNull = sessionRepository.findOneById(null);
        Assert.assertNull(foundSessionNull);
    }

    @Test
    @DisplayName("Поиск сессии по Id для пользователя")
    public void findOneByIdForUserTest() {
        @Nullable Session session;
        @NotNull List<Session> sessionListForUser = sessionList
                .stream()
                .filter(m -> USER_1_ID.equals(m.getUserId()))
                .collect(Collectors.toList());
        for (int i = 0; i < sessionListForUser.size(); i++) {
            session = sessionRepository.findOneByIndex(USER_1_ID, i);
            Assert.assertNotNull(session);
            @NotNull final String sessionId = session.getId();
            @Nullable final Session foundSession = sessionRepository.findOneById(USER_1_ID, sessionId);
            Assert.assertNotNull(foundSession);
        }
    }

    @Test
    @DisplayName("Поиск сессии по Null Id для пользователя")
    public void findOneByIdNullForUserTest() {
        @Nullable final Session foundSession = sessionRepository.findOneById(USER_1_ID, "qwerty");
        Assert.assertNull(foundSession);
        @Nullable final Session foundSessionNull = sessionRepository.findOneById(USER_1_ID, null);
        Assert.assertNull(foundSessionNull);
    }

    @Test
    @DisplayName("Поиск сессии по индексу")
    public void findOneByIndexTest() {
        for (int i = 0; i < sessionList.size(); i++) {
            @Nullable final Session session = sessionRepository.findOneByIndex(i);
            Assert.assertNotNull(session);
        }
    }

    @Test
    @DisplayName("Поиск сессии по Null индексу")
    public void findOneByIndexNullTest() {
        @Nullable final Session session = sessionRepository.findOneByIndex(null);
        Assert.assertNull(session);
    }

    @Test
    @DisplayName("Поиск сессии по индексу для пользователя")
    public void findOneByIndexForUserTest() {
        @NotNull List<Session> sessionListForUser = sessionList
                .stream()
                .filter(m -> USER_1_ID.equals(m.getUserId()))
                .collect(Collectors.toList());
        for (int i = 0; i < sessionListForUser.size(); i++) {
            @Nullable final Session session = sessionRepository.findOneByIndex(USER_1_ID, i);
            Assert.assertNotNull(session);
        }
    }

    @Test
    @DisplayName("Поиск сессии по Null индексу для пользователя")
    public void findOneByIndexNullForUserText() {
        @Nullable final Session session = sessionRepository.findOneByIndex(USER_1_ID, null);
        Assert.assertNull(session);
    }

    @Test
    @DisplayName("Получение количества сессий")
    public void getSizeTest() {
        int expectedSize = sessionList.size();
        int actualSize = sessionRepository.getSize();
        Assert.assertEquals(expectedSize, actualSize);
    }

    @Test
    @DisplayName("Получение количества сессий для пользователя")
    public void getSizeForUserTest() {
        int expectedSize = (int) sessionList
                .stream()
                .filter(m -> USER_1_ID.equals(m.getUserId()))
                .count();
        int actualSize = sessionRepository.getSize(USER_1_ID);
        Assert.assertEquals(expectedSize, actualSize);
    }

    @Test
    @DisplayName("Удаление пустого списка сессий")
    public void removeAllNullTest() {
        int expectedNumberOfEntries = sessionRepository.getSize();
        sessionRepository.removeAll(null);
        int actualNumberOfEntries = sessionRepository.getSize();
        Assert.assertEquals(expectedNumberOfEntries, actualNumberOfEntries);
    }

    @Test
    @DisplayName("Удаление сессии")
    public void removeTest() {
        @Nullable final Session session = sessionRepository.findOneByIndex(0);
        Assert.assertNotNull(session);
        @NotNull final String sessionId = session.getId();
        @Nullable final Session deletedSession = sessionRepository.remove(session);
        Assert.assertNotNull(deletedSession);
        @Nullable final Session deletedSessionInRepository = sessionRepository.findOneById(sessionId);
        Assert.assertNull(deletedSessionInRepository);
    }

    @Test
    @DisplayName("Удаление Null сессии")
    public void removeNullTest() {
        @Nullable final Session session = sessionRepository.remove(null);
        Assert.assertNull(session);
    }

    @Test
    @DisplayName("Удаление сессии по Id")
    public void removeByIdTest() {
        int index = sessionList.size();
        while (index > 0) {
            @Nullable final Session session = sessionRepository.findOneByIndex(index - 1);
            Assert.assertNotNull(session);
            @NotNull final String sessionId = session.getId();
            @Nullable final Session deletedSession = sessionRepository.removeById(sessionId);
            Assert.assertNotNull(deletedSession);
            @Nullable final Session deletedSessionInRepository = sessionRepository.findOneById(sessionId);
            Assert.assertNull(deletedSessionInRepository);
            index--;
        }
    }

    @Test
    @DisplayName("Удаление сессии по Id равному Null")
    public void removeByIdNullTest() {
        @Nullable final Session deletedSession = sessionRepository.removeById("qwerty");
        Assert.assertNull(deletedSession);
        @Nullable final Session deletedSessionNull = sessionRepository.removeById(null);
        Assert.assertNull(deletedSessionNull);
    }

    @Test
    @DisplayName("Удаление сессии по Id для пользователя")
    public void removeByIdForUserTest() {
        int index = (int) sessionList
                .stream()
                .filter(m -> USER_1_ID.equals(m.getUserId()))
                .count();
        while (index > 0) {
            @Nullable final Session session = sessionRepository.findOneByIndex(USER_1_ID, index - 1);
            Assert.assertNotNull(session);
            @NotNull final String sessionId = session.getId();
            @Nullable final Session deletedSession = sessionRepository.removeById(USER_1_ID, sessionId);
            Assert.assertNotNull(deletedSession);
            @Nullable final Session deletedSessionInRepository = sessionRepository.findOneById(USER_1_ID, sessionId);
            Assert.assertNull(deletedSessionInRepository);
            index--;
        }
    }

    @Test
    @DisplayName("Удаление сессии по Id равному Null для пользователя")
    public void removeByIdNullForUserTest() {
        @Nullable final Session deletedSession = sessionRepository.removeById(USER_1_ID, "qwerty");
        Assert.assertNull(deletedSession);
        @Nullable final Session deletedSessionNull = sessionRepository.removeById(USER_1_ID, null);
        Assert.assertNull(deletedSessionNull);
    }

    @Test
    @DisplayName("Удаление сессии по индексу")
    public void removeByIndexTest() {
        int index = sessionList.size();
        while (index > 0) {
            @Nullable final Session deletedSession = sessionRepository.removeByIndex(index - 1);
            Assert.assertNotNull(deletedSession);
            @NotNull final String sessionId = deletedSession.getId();
            @Nullable final Session deletedSessionInRepository = sessionRepository.findOneById(sessionId);
            Assert.assertNull(deletedSessionInRepository);
            index--;
        }
    }

    @Test
    @DisplayName("Удаление сессии по индексу равному Null")
    public void removeByIndexNullTest() {
        @Nullable final Session deletedSession = sessionRepository.removeByIndex(null);
        Assert.assertNull(deletedSession);
    }

    @Test
    @DisplayName("Удаление сессии по индексу для пользователя")
    public void removeByIndexForUserTest() {
        int index = (int) sessionList
                .stream()
                .filter(m -> USER_1_ID.equals(m.getUserId()))
                .count();
        while (index > 0) {
            @Nullable final Session deletedSession = sessionRepository.removeByIndex(USER_1_ID, index - 1);
            Assert.assertNotNull(deletedSession);
            @NotNull final String sessionId = deletedSession.getId();
            @Nullable final Session deletedSessionInRepository = sessionRepository.findOneById(USER_1_ID, sessionId);
            Assert.assertNull(deletedSessionInRepository);
            index--;
        }
    }

    @Test
    @DisplayName("Удаление сессии по индексу равному Null для пользователя")
    public void removeByIndexNullForUserTest() {
        @Nullable final Session deletedSession = sessionRepository.removeByIndex(USER_1_ID, null);
        Assert.assertNull(deletedSession);
    }

    @Test
    @DisplayName("Установить список сессий")
    public void setTest() {
        int expectedNumberOfEntries = 2;
        @NotNull final List<Session> sessions = new ArrayList<>();
        @NotNull final Session firstSession = new Session();
        sessions.add(firstSession);
        @NotNull final Session secondSession = new Session();
        sessions.add(secondSession);
        @NotNull final Collection<Session> addedSessions = sessionRepository.set(sessions);
        Assert.assertTrue(addedSessions.size() > 0);
        int actualNumberOfEntries = sessionRepository.getSize();
        Assert.assertEquals(expectedNumberOfEntries, actualNumberOfEntries);
    }

}