package ru.t1.panasyuk.tm.api.service;

import ru.t1.panasyuk.tm.model.Session;

public interface ISessionService extends IUserOwnedService<Session> {
}
