package ru.t1.panasyuk.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.t1.panasyuk.tm.api.endpoint.IDomainEndpoint;
import ru.t1.panasyuk.tm.api.service.IServiceLocator;
import ru.t1.panasyuk.tm.dto.request.data.*;
import ru.t1.panasyuk.tm.dto.response.data.*;
import ru.t1.panasyuk.tm.enumerated.Role;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@NoArgsConstructor
@WebService(endpointInterface = "ru.t1.panasyuk.tm.api.endpoint.IDomainEndpoint")
public final class DomainEndpoint extends AbstractEndpoint implements IDomainEndpoint {

    public DomainEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    @Override
    @WebMethod
    public DataBackupLoadResponse loadDataBackup(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataBackupLoadRequest request
    ) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().loadDataBackup();
        return new DataBackupLoadResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataBackupSaveResponse saveDataBackup(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataBackupSaveRequest request
    ) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().saveDataBackup();
        return new DataBackupSaveResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataBase64LoadResponse loadDataBase64(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataBase64LoadRequest request
    ) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().loadDataBase64();
        return new DataBase64LoadResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataBase64SaveResponse saveDataBase64(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataBase64SaveRequest request
    ) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().saveDataBase64();
        return new DataBase64SaveResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataBinaryLoadResponse loadDataBinary(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataBinaryLoadRequest request
    ) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().loadDataBinary();
        return new DataBinaryLoadResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataBinarySaveResponse saveDataBinary(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataBinarySaveRequest request
    ) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().saveDataBinary();
        return new DataBinarySaveResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataJsonLoadFasterXMLResponse loadDataJsonFasterXML(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataJsonLoadFasterXMLRequest request
    ) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().loadDataJsonFasterXML();
        return new DataJsonLoadFasterXMLResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataJsonLoadJaxBResponse loadDataJsonJaxB(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataJsonLoadJaxBRequest request
    ) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().loadDataJsonJaxB();
        return new DataJsonLoadJaxBResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataJsonSaveFasterXMLResponse saveDataJsonFasterXML(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataJsonSaveFasterXMLRequest request
    ) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().saveDataJsonFasterXML();
        return new DataJsonSaveFasterXMLResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataJsonSaveJaxBResponse saveDataJsonJaxB(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataJsonSaveJaxBRequest request
    ) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().saveDataJsonJaxB();
        return new DataJsonSaveJaxBResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataXMLLoadFasterXMLResponse loadDataXMLFasterXML(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataXMLLoadFasterXMLRequest request
    ) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().loadDataXMLFasterXML();
        return new DataXMLLoadFasterXMLResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataXMLLoadJaxBResponse loadDataXMLJaxB(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataXMLLoadJaxBRequest request
    ) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().loadDataXMLJaxB();
        return new DataXMLLoadJaxBResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataXMLSaveFasterXMLResponse saveDataXMLFasterXML(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataXMLSaveFasterXMLRequest request
    ) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().saveDataXMLFasterXML();
        return new DataXMLSaveFasterXMLResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataXMLSaveJaxBResponse saveDataXMLJaxB(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataXMLSaveJaxBRequest request
    ) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().saveDataXMLJaxB();
        return new DataXMLSaveJaxBResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataYamlLoadFasterXMLResponse loadDataYamlFasterXML(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataYamlLoadFasterXMLRequest request
    ) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().loadDataYamlFasterXML();
        return new DataYamlLoadFasterXMLResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataYamlSaveFasterXMLResponse saveDataYamlFasterXML(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataYamlSaveFasterXMLRequest request
    ) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().saveDataYamlFasterXML();
        return new DataYamlSaveFasterXMLResponse();
    }

}
