package ru.t1.panasyuk.tm.exception.system;

import org.jetbrains.annotations.NotNull;

public final class ArgumentNotSupportedException extends AbstractSystemException {

    public ArgumentNotSupportedException() {
        super("Error! Argument not supported...");
    }

    public ArgumentNotSupportedException(@NotNull final String argument) {
        super("Error! argument \"" + argument + "\" not supported");
    }

}