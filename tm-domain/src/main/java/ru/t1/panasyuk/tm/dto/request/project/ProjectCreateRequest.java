package ru.t1.panasyuk.tm.dto.request.project;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.panasyuk.tm.dto.request.AbstractUserRequest;

@Getter
@Setter
@NoArgsConstructor
public final class ProjectCreateRequest extends AbstractUserRequest {

    @Nullable
    private String name;

    @Nullable
    private String description;

    public ProjectCreateRequest(@Nullable final String name, @Nullable final String description) {
        this.name = name;
        this.description = description;
    }

    public ProjectCreateRequest(
            @Nullable final String token,
            @Nullable final String name,
            @Nullable final String description
    ) {
        super(token);
        this.name = name;
        this.description = description;
    }

}