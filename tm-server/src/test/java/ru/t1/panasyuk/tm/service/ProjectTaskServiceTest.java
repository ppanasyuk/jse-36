package ru.t1.panasyuk.tm.service;

import io.qameta.allure.junit4.DisplayName;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.t1.panasyuk.tm.api.repository.IProjectRepository;
import ru.t1.panasyuk.tm.api.repository.ITaskRepository;
import ru.t1.panasyuk.tm.api.repository.IUserRepository;
import ru.t1.panasyuk.tm.api.service.*;
import ru.t1.panasyuk.tm.enumerated.Role;
import ru.t1.panasyuk.tm.exception.entity.ProjectNotFoundException;
import ru.t1.panasyuk.tm.exception.entity.TaskNotFoundException;
import ru.t1.panasyuk.tm.exception.field.IndexIncorrectException;
import ru.t1.panasyuk.tm.exception.field.ProjectIdEmptyException;
import ru.t1.panasyuk.tm.exception.field.TaskIdEmptyException;
import ru.t1.panasyuk.tm.model.Project;
import ru.t1.panasyuk.tm.model.Task;
import ru.t1.panasyuk.tm.model.User;
import ru.t1.panasyuk.tm.repository.ProjectRepository;
import ru.t1.panasyuk.tm.repository.TaskRepository;
import ru.t1.panasyuk.tm.repository.UserRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@DisplayName("Тестирование сервиса ProjectTaskService")
public class ProjectTaskServiceTest {

    @NotNull
    private List<Project> projectList;

    @NotNull
    private IProjectService projectService;

    @NotNull
    private ITaskService taskService;

    @NotNull
    private IProjectTaskService projectTaskService;

    @NotNull
    private User test;

    @NotNull
    private User admin;

    @Before
    public void initService() {
        @NotNull final IProjectRepository projectRepository = new ProjectRepository();
        @NotNull final IUserRepository userRepository = new UserRepository();
        @NotNull final ITaskRepository taskRepository = new TaskRepository();
        @NotNull final IPropertyService propertyService = new PropertyService();
        projectService = new ProjectService(projectRepository);
        taskService = new TaskService(taskRepository);
        projectTaskService = new ProjectTaskService(projectRepository, taskRepository);
        @NotNull final IUserService userService = new UserService(propertyService, userRepository, projectTaskService);
        projectList = new ArrayList<>();
        test = userService.create("TEST", "TEST", "ppanasyuk@t1-consulting.ru");
        admin = userService.create("ADMIN", "ADMIN", Role.ADMIN);
        @NotNull final Project project1 = projectService.create(test.getId(), "Project 1", "Project for TEST");
        @NotNull final Project project2 = projectService.create(admin.getId(), "Project 2", "Project for ADMIN");
        @NotNull final Project project3 = projectService.create(test.getId(), "Project 3", "Project for TEST 2");
        @NotNull final Task task1 = taskService.create(test.getId(), "Task 1", "Task for project 1");
        task1.setProjectId(project1.getId());
        @NotNull final Task task2 = taskService.create(test.getId(), "Task 2", "Task for project 1");
        task2.setProjectId(project1.getId());
        @NotNull final Task task3 = taskService.create(admin.getId(), "Task 3", "Task for project 2");
        task3.setProjectId(project2.getId());
        @NotNull final Task task4 = taskService.create(admin.getId(), "Task 4", "Task for project 2");
        task4.setProjectId(project2.getId());
        projectList.add(project1);
        projectList.add(project2);
        projectList.add(project3);
    }

    @Test
    @DisplayName("Связать задачу с проектом")
    public void bindTaskToProjectTest() {
        @NotNull final List<Project> projects = projectService.findAll(test.getId());
        Assert.assertTrue(projects.size() > 0);
        @NotNull final Project project = projects.get(0);
        int expectedNumberOfEntries = taskService.findAllByProjectId(test.getId(), project.getId()).size() + 1;
        @NotNull final Task newTask = taskService.create(test.getId(), "Test task", "Task for test");
        @NotNull final Task boundTask = projectTaskService.bindTaskToProject(test.getId(), project.getId(), newTask.getId());
        Assert.assertNotNull(boundTask);
        int newNumberOfEntries = taskService.findAllByProjectId(test.getId(), project.getId()).size();
        Assert.assertEquals(expectedNumberOfEntries, newNumberOfEntries);
    }

    @Test(expected = ProjectIdEmptyException.class)
    @DisplayName("Связать задачу с проектом по Null Id проекта")
    public void bindTaskToProjectProjectIdNullTestNegative() {
        projectTaskService.bindTaskToProject(test.getId(), null, "TASK_ID");
    }

    @Test(expected = ProjectIdEmptyException.class)
    @DisplayName("Связать задачу с проектом по пустому Id проекта")
    public void bindTaskToProjectProjectIdEmptyTestNegative() {
        projectTaskService.bindTaskToProject(test.getId(), "", "TASK_ID");
    }

    @Test(expected = TaskIdEmptyException.class)
    @DisplayName("Связать задачу с проектом по Null Id задачи")
    public void bindTaskToProjectTaskIdNullTestNegative() {
        projectTaskService.bindTaskToProject(test.getId(), "PROJECT_ID", null);
    }

    @Test(expected = TaskIdEmptyException.class)
    @DisplayName("Связать задачу с проектом по путсому Id задачи")
    public void bindTaskToProjectTaskIdEmptyTestNegative() {
        projectTaskService.bindTaskToProject(test.getId(), "PROJECT_ID", "");
    }

    @Test(expected = ProjectNotFoundException.class)
    @DisplayName("Связать задачу с несуществующим проектом")
    public void bindTaskToProjectProjectNotFoundTestNegative() {
        projectTaskService.bindTaskToProject(test.getId(), "PROJECT_ID", "123321");
    }

    @Test(expected = TaskNotFoundException.class)
    @DisplayName("Связать несуществующую задачу с проектом")
    public void bindTaskToProjectTaskNotFoundTestNegative() {
        @NotNull final String projectId = projectList.get(0).getId();
        projectTaskService.bindTaskToProject(test.getId(), projectId, "123321");
    }

    @Test
    @DisplayName("Удалить проект по Id")
    public void removeProjectByIdTest() {
        @NotNull final List<Project> projects = projectList
                .stream()
                .filter(m -> test.getId().equals(m.getUserId()))
                .collect(Collectors.toList());
        Assert.assertTrue(projects.size() > 0);
        for(@NotNull final Project project : projects) {
            @NotNull final Project deletedProject = projectTaskService.removeProjectById(test.getId(), project.getId());
            Assert.assertNotNull(deletedProject);
        }
        @NotNull final List<Project> projectsAfterRemoving = projectService.findAll(test.getId());
        Assert.assertEquals(0, projectsAfterRemoving.size());
        @NotNull final List<Task> tasksAfterRemoving = taskService.findAll(test.getId());
        Assert.assertEquals(0, tasksAfterRemoving.size());
    }

    @Test(expected = ProjectIdEmptyException.class)
    @DisplayName("Удалить проект по Null Id")
    public void removeProjectByIdProjectIdNullTestNegative() {
        projectTaskService.removeProjectById(test.getId(), null);
    }

    @Test(expected = ProjectIdEmptyException.class)
    @DisplayName("Удалить проект по пустому Id")
    public void removeProjectByIdProjectIdEmptyTestNegative() {
        projectTaskService.removeProjectById(test.getId(), "");
    }

    @Test(expected = ProjectNotFoundException.class)
    @DisplayName("Удалить несуществующий проект по Id")
    public void removeProjectByIdProjectNotFoundTestNegative() {
        projectTaskService.removeProjectById(test.getId(), "123321");
    }

    @Test
    @DisplayName("Удалить проект по индексу")
    public void removeProjectByIndexTest() {
        @NotNull final List<Project> projects = projectList
                .stream()
                .filter(m -> test.getId().equals(m.getUserId()))
                .collect(Collectors.toList());
        Assert.assertTrue(projects.size() > 0);
        int index = projects.size() - 1;
        while(index >= 0) {
            @NotNull final Project deletedProject = projectTaskService.removeProjectByIndex(test.getId(), index);
            Assert.assertNotNull(deletedProject);
            index--;
        }
        @NotNull final List<Project> projectsAfterRemoving = projectService.findAll(test.getId());
        Assert.assertEquals(0, projectsAfterRemoving.size());
        @NotNull final List<Task> tasksAfterRemoving = taskService.findAll(test.getId());
        Assert.assertEquals(0, tasksAfterRemoving.size());
    }

    @Test(expected = IndexIncorrectException.class)
    @DisplayName("Удалить проект по Null индексу")
    public void removeProjectByIndexNullTestNegative() {
        projectTaskService.removeProjectByIndex(test.getId(), null);
    }

    @Test(expected = IndexIncorrectException.class)
    @DisplayName("Удалить проект по отрицательному индексу")
    public void removeProjectByIndexMinusTestNegative() {
        projectTaskService.removeProjectByIndex(test.getId(), -1);
    }

    @Test(expected = IndexIncorrectException.class)
    @DisplayName("Удалить проект по индексу превышающему количество задач")
    public void removeProjectByIndexTestNegative() {
        projectTaskService.removeProjectByIndex(test.getId(), projectList.size() + 1);
    }

    @Test
    @DisplayName("Удалить все проекты")
    public void clearProjectTest() {
        int numberOfProjects = projectService.getSize(test.getId());
        Assert.assertTrue(numberOfProjects > 0);
        int numberOfTasks = taskService.getSize(test.getId());
        Assert.assertTrue(numberOfTasks > 0);
        projectTaskService.clearProjects(test.getId());
        int numberOfProjectsAfterRemoving = projectService.getSize(test.getId());
        Assert.assertEquals(0, numberOfProjectsAfterRemoving);
        int numberOfTasksAfterRemoving = taskService.getSize(test.getId());
        Assert.assertEquals(0, numberOfTasksAfterRemoving);
    }

    @Test
    @DisplayName("Отвязать задачу от проекта")
    public void unbindTaskFromProjectTest() {
        @NotNull final List<Project> projects = projectService.findAll(test.getId());
        Assert.assertTrue(projects.size() > 0);
        @NotNull final Project project = projects.get(0);
        @NotNull final List<Task> tasks = taskService.findAllByProjectId(test.getId(), project.getId());
        @NotNull final String taskForUnbindId = tasks.get(0).getId();
        @Nullable final Task boundTask = projectTaskService.bindTaskToProject(test.getId(), project.getId(), taskForUnbindId);
        Assert.assertNotNull(boundTask);
        @Nullable Task unboundTask = projectTaskService.unbindTaskFromProject(test.getId(), boundTask.getProjectId(), boundTask.getId());
        Assert.assertNotNull(unboundTask);
        unboundTask = taskService.findOneById(unboundTask.getId());
        Assert.assertNotNull(unboundTask);
        Assert.assertNull(unboundTask.getProjectId());
    }

    @Test(expected = ProjectIdEmptyException.class)
    @DisplayName("Отвязать задачу от проекта по Null Id проекта")
    public void unbindTaskFromProjectProjectIdNullTestNegative() {
        projectTaskService.unbindTaskFromProject(test.getId(), null, "TASK_ID");
    }

    @Test(expected = ProjectIdEmptyException.class)
    @DisplayName("Отвязать задачу от проекта по пустому Id проекта")
    public void unbindTaskFromProjectProjectIdEmptyTestNegative() {
        projectTaskService.unbindTaskFromProject(test.getId(), "", "TASK_ID");
    }

    @Test(expected = TaskIdEmptyException.class)
    @DisplayName("Отвязать задачу от проекта по Null Id задачи")
    public void unbindTaskFromProjectTaskIdNullTestNegative() {
        projectTaskService.unbindTaskFromProject(test.getId(), "PROJECT_ID", null);
    }

    @Test(expected = TaskIdEmptyException.class)
    @DisplayName("Отвязать задачу от проекта по пустому Id задачи")
    public void unbindTaskFromProjectTaskIdEmptyTestNegative() {
        projectTaskService.unbindTaskFromProject(test.getId(), "PROJECT_ID", "");
    }

    @Test(expected = ProjectNotFoundException.class)
    @DisplayName("Отвязать задачу от несуществующего проекта")
    public void unbindTaskFromProjectProjectNotFoundTestNegative() {
        projectTaskService.unbindTaskFromProject(test.getId(), "PROJECT_ID", "123321");
    }

    @Test(expected = TaskNotFoundException.class)
    @DisplayName("Отвязать несуществующую задачу от проекта")
    public void unbindTaskFromProjectTaskNotFoundTestNegative() {
        @NotNull final String projectId = projectList.get(0).getId();
        projectTaskService.unbindTaskFromProject(test.getId(), projectId, "123321");
    }

}